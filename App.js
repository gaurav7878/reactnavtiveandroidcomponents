/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  TextInput,
  View,
} from 'react-native';

import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  StyleProvider,
} from 'native-base';

import AppContainer from './src/navigations/AppNavigation';

const App = () => {
  return <AppContainer />;
};

const styles = StyleSheet.create({
  CardHieghtWidth: {
    height: 100,
  },
});

export default App;
